package someotherstyles

import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.ints.shouldBeExactly

class FeatureExample : FeatureSpec({
    feature("feature: addition") {
        scenario("scenario: 1 + 1 should be 2") {
            1 + 1 shouldBeExactly 2
        }
        scenario("scenario: 3 + 2 should be 5") {
            3 + 2 shouldBeExactly 5
        }
    }
    feature("feature: subtraction") {
        scenario("scenario: 1 - 1 should be 0") {
            1 - 1 shouldBeExactly 0
        }
        scenario("scenario: 3 - 2 should be 1") {
            3 - 2 shouldBeExactly 1
        }
    }
})