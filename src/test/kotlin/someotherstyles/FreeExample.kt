package someotherstyles

import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.ints.shouldBeExactly
import io.kotest.matchers.shouldNotBe

class FreeExample : FreeSpec({
    "some stuff" - {
        "nest" - {
            "nest... wait for it" - { // the resulting name... hmmmm...
                "nest, and finally" - {
                    "test" - {
                        1 shouldBeExactly 1
                    }
                }
            }
        }
    }
    "other stuff" - {
        "nest" - {
            "test" - {
                3 shouldNotBe 4
            }
        }
    }
})