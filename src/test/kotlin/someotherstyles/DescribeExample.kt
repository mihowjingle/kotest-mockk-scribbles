package someotherstyles

import cartesian.times
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.collections.beEmpty
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.property.forAll as alsoForAll // import conflict? import alias to the rescue!

class DescribeExample : DescribeSpec({
    describe("addition") {
        listOf(
            Triple(1, 0, 1),
            Triple(1, 1, 2),
            Triple(7, 3, 10)
        ).forEach { (a, b, expected) ->
            it("should confirm $a + $b = $expected") { // data-driven tests, n cases -> n tests, also, flexibility to use your own case/row structure
                a + b shouldBe expected
            }
        }
        it("should play with property-based testing too (and confirm addition of integers is commutative, btw)") {
            alsoForAll<Int, Int> { a, b -> a + b == b + a } // checkAll is better, gives more descriptive failure messages, thanks to using actual matchers
        }
    }
    describe("other stuff") {
        it("cartesian product of two empty Iterables should be empty") { // it... should... what? sometimes it's impossible to make it sound like actual english
            listOf<Any>() * setOf<Any>() should beEmpty()
        }
        it("should concatenate strings properly") {
            forAll( // data-driven tests, n cases -> 1 test, using the row() function, which is really the only option in this case
                row("a", "b", "c", "abc"),
                row("hel", "lo wo", "rld", "hello world"),
                row("", "z", "", "z")
            ) { a, b, c, d -> a + b + c shouldBe d }
        }
    }
})