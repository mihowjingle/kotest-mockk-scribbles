package cartesian

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import io.kotest.property.checkAll

class CartesianSpec : StringSpec({
    data class Case(val first: Iterable<Any>, val second: Iterable<Any>, val expectedResult: List<Pair<Any, Any>>) // to have nice explicit argument names instead of row(a, b, c... )
    listOf( // kotest: data-driven testing
        Case(
            first = setOf(1, 2, 3),
            second = listOf(3, 4),
            expectedResult = listOf(Pair(1, 3), Pair(1, 4), Pair(2, 3), Pair(2, 4), Pair(3, 3), Pair(3, 4))
        ),
        Case(
            first = listOf('a', 'b', 'c'),
            second = setOf(true, false),
            expectedResult = listOf(Pair('a', true), Pair('a', false), Pair('b', true), Pair('b', false), Pair('c', true), Pair('c', false))
        ),
        Case(
            first = listOf(),
            second = listOf(),
            expectedResult = listOf()
        ),
        Case(
            first = setOf(7, 8, 9),
            second = setOf(),
            expectedResult = listOf()
        ),
        Case(
            first = listOf(),
            second = listOf("whatever", "non", "empty", "list"),
            expectedResult = listOf()
        )
    ).forEach { (first, second, expectedResult) ->
        "cartesian product of $first and $second should contain exactly $expectedResult in any order" {
            first * second shouldContainExactlyInAnyOrder expectedResult
        }
    }
    "the length of a cartesian product should be equal to the product of the lengths of the argument Iterables" { // kotest: property-based testing
        checkAll<List<Int>, List<Int>>(iterations = 100) { first, second -> (first * second).size shouldBe first.size * second.size }
    }
})