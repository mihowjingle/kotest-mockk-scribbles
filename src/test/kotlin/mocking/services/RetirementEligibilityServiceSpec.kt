package mocking.services

import io.kotest.core.spec.style.FunSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import mocking.models.Person
import java.time.LocalDate

internal class RetirementEligibilityServiceSpec : FunSpec({
    context("it should notify the right people that they are eligible for retirement") {

        val personRepository: PersonRepository = mockk() // this way...
        val mailService = spyk<MailService>() // ...or this way
        val testedService = RetirementEligibilityService(personRepository, mailService)

        test("no people (at all) -> (definitely) no emails") {
            every { personRepository.findAllPeople() } returns listOf()
            testedService.notifyAllPeopleEligibleForRetirement()
            verify(exactly = 0) { mailService.sendEmail(any(), any()) }
        }

        test("no people (eligible for retirement) -> (also) no emails") {
            every { personRepository.findAllPeople() } returns listOf(
                Person(1, "Stefan", "Huba", LocalDate.of(1990, 1, 9), Person.Gender.MALE),
                Person(2, "Maria", "Muchomor", LocalDate.of(1982, 11, 19), Person.Gender.FEMALE),
                Person(3, "Katarzyna", "Kania", LocalDate.of(1977, 5, 22), Person.Gender.FEMALE)
            )
            testedService.notifyAllPeopleEligibleForRetirement()
            verify(exactly = 0) { mailService.sendEmail(any(), any()) }
        }

        test("2 out of 3 people eligible for retirement -> 2 emails") {
            every { personRepository.findAllPeople() } returns listOf(
                Person(1, "Stefan", "Huba", LocalDate.of(1950, 1, 9), Person.Gender.MALE),
                Person(2, "Maria", "Muchomor", LocalDate.of(1982, 11, 19), Person.Gender.FEMALE),
                Person(3, "Katarzyna", "Kania", LocalDate.of(1947, 5, 22), Person.Gender.FEMALE)
            )
            testedService.notifyAllPeopleEligibleForRetirement()
            verify(exactly = 1) { mailService.sendEmail(to = match { person -> person.id == 1L }, content = "Hey, you're retired! :)") }
            verify(exactly = 1) { mailService.sendEmail(to = match { person -> person.id == 3L }, content = "Hey, you're retired! :)") }
        }
    }
})