package cartesian

fun <A, B> cartesian(one: Iterable<A>, other: Iterable<B>): List<Pair<A, B>> {
    val list = mutableListOf<Pair<A, B>>()
    for (a in one) {
        for (b in other) {
            list.add(Pair(a, b))
        }
    }
    return list
}

operator fun <A, B> Iterable<A>.times(that: Iterable<B>) = cartesian(this, that)