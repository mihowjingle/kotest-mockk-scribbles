package mocking.services

import mocking.models.Person

class RetirementEligibilityService(private val personRepository: PersonRepository, private val mailService: MailService) {

    private fun eligibleForRetirement(person: Person) = person.age >= 65 || person.age >= 60 && person.gender == Person.Gender.FEMALE

    fun notifyAllPeopleEligibleForRetirement() =
        personRepository.findAllPeople()
            .filter(::eligibleForRetirement)
            .forEach { mailService.sendEmail(it, "Hey, you're retired! :)") }
}