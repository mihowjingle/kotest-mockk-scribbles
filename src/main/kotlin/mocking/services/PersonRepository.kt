package mocking.services

import mocking.models.Person

object PersonRepository {
    fun findAllPeople(): List<Person> = throw NotImplementedError("this will be mocked anyway")
}