package mocking.services

import mocking.models.Person

object MailService {
    fun sendEmail(to: Person, content: String) = println("Sending email to ${to.firstName} ${to.lastName} -> $content")
}