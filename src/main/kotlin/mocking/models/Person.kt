package mocking.models

import java.time.LocalDate
import java.time.Period

data class Person(
    val id: Long?,
    val firstName: String,
    val lastName: String,
    val dateOfBirth: LocalDate,
    val gender: Gender
) {
    val age get() = Period.between(dateOfBirth, LocalDate.now()).years

    enum class Gender {
        MALE,
        FEMALE
    }
}